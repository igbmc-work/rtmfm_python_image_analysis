# Data Source

## Protein atlas

Data source: https://github.com/ahklemm/ImageJMacro_Introduction
Original source: [human protein atlas](https://www.proteinatlas.org/)

## Tau lif

Demo lifetime image from INCI (Strasbourg).

## Green segmentation

Arabidopsis thaliana plant photograph (CEA Cadarache).

## Flatfield correction

Flatfield images are from the _Introduction to Image Processing & Analysis with Fiji_ training course organized by RISEst [here](https://seafile.unistra.fr/d/415685f745de42feaab6/).

## Colocalization

Data was taken from this sample application of [this Colocalization Analysis plugin](https://imagej.net/imaging/colocalization-analysis#colocalization-threshold) in ImageJ.