# RT-mfm training: Python & Git for bioimage analysis

Data analysis and machine learning are becoming an essential skill for every scientist. Additionally, with microscopy and other imaging modalities as ever-growing tools in scientific research, so grows the need to extract quantitative and precise information from simple to large multidimensional images and videos. This course provides an Image Analysis oriented introduction to Python, a widely used scientific programming language for those tasks. We will also introduce the basic concepts of code versioning via Git using GitLab and GitHub in the context of coding for image analysis. Possibility to organize  one discussion about the deployment of Gitlab with the IT department of IGBMC.

## Before the course

Please take a look at the information before the beginning of the course [here](https://gitlab.com/igbmc/mic-photon/rtmfm_python_image_analysis/-/blob/main/extra/before_going_to_training.pdf).

## Course Material

- Slides for the Git and GitLab course material are available [here](https://gitlab.com/igbmc/mic-photon/rtmfm_python_image_analysis/-/blob/main/extra/git2023.pdf)
- Slides for the Python course material are available at [this link](https://docs.google.com/presentation/d/1X1zNkIZLUqLq3UDV9Kx354j75qfb1ghYOB3pj1z42SA/edit?usp=sharing)
- Notebooks are available in the notebook folder of this repository

## Dates

23rd to 25th May 2023

## Current Program

| **Time**    | **Day** | **Topic**                                           |
|-------------|---------|-----------------------------------------------------|
| 9.30-10.30  | 23.05   | Version control Git                                 |
| 10.30-11.30 | 23.05   | Version control Git                                 |
| 11.30-12.30 | 23.05   | setup course material from course repo + conda envs |
| 12.30-13.30 | 23.05   | LUNCH                                               |
| 13.30-14.30 | 23.05   | Python basics                                       |
| 14.30-15.30 | 23.05   | Python basics                                       |
| 15.30-16.30 | 23.05   | Python basics                                       |
| 16.30-17.30 | 23.05   | Python basics                                       |
| 9.30-10.30  | 24.05   | Python basics reminder + functions                  |
| 10.30-11.00 | 24.05   | Imaris DEMO                                         |
| 11.00-12.30 | 24.05   | Numpy (imgs as array)                               |
| 12.30-13.30 | 24.05   | LUNCH                                               |
| 13.30-14.30 | 24.05   | Scikit-image (Stardist/Cellpose)                    |
| 14.30-15.30 | 24.05   | Scikit-image                                        |
| 15.30-16.30 | 24.05   | Scikit-image                                        |
| 16.30-17.30 | 24.05   | Scikit-image                                        |
| evening     | 24.05   | DINNER                                              |
| 9.30-10.30  | 25.05   | Pandas + Dict + Numpy                               |
| 10.30-11.30 | 25.05   | Matplotlib                                          |
| 11.30-12.30 | 25.05   | Matplotlib/Seaborn                                  |
| 12.30-13.30 | 25.05   | LUNCH                                               |
| 13.30-14.30 | 25.05   | Applications: Napari                                |
| 14.30-15.30 | 25.05   | Applications: OAD Zeiss Demo                        |
| 15.30-16.30 | 25.05   | Applications: aicsimageio (bioformat equivalent)    |
| 16.30-17.30 | 25.05   | Applications: OpenCV                                |

## License

This material is available and reusable under a non-commercial Creative Commons license. Please read the terms and conditions in the LICENSE file for more informaiton.
